#Binome
#BOUKARI Idir   //   BENAISSA Sidahmed
#projet Reussite

from tkinter import * 
from random import *
import time 
from tkinter import ttk as ttk
import pygame  #juste pour ajouter du son car les fonction d'ajout de son de tkinter bug sur linux
import os
import tkinter.font as tkfont
 


#  Initialisation des donnèes 
root = Tk()
nbr = 0
sound = True
musique = True
etat = []
Recomancer = []
tabbtn = []
tablabel = []
tap = 0
id1 = 0
col1 = 0
#  Creation de la fenêtre principale 
root.geometry("1700x800")
root.resizable(height = False, width = False )
root.title("The best Reussite game ever")  

# fonction qui permet d'utiliser le son des cartes 
def musicInit():
        global nbr 
        nbr += 1
        if nbr < 10:
            root.after(100,musicInit)
        else: nbr =0
        pygame.mixer.Channel(1).play(pygame.mixer.Sound("musiques/Card-flip-sound-effect.wav"))

# fonction qui permet d'utiliser la musique du fond  
def musique_Fond():
    global musique
    if musique :
        pygame.mixer.Channel(0).play(pygame.mixer.Sound("musiques/musique_fond.wav"),loops =-1)

# fonction qui permet d'utiliser le son des cartes  
def son_carte():
    if sound:
        pygame.mixer.Channel(1).play(pygame.mixer.Sound("musiques/Card-flip-sound-effect.wav"))

# fonction qui permet de vèrifier si le joueur a gagner ou pas 
def gagner():
    global etat
    compteur = 0
    for i in range(7):
        if len(etat[i]) != 0:
            compteur+=1
    if compteur > 4:
        return False
    for i in [7,8,9,10,11,12]:
        if  len(etat[i]) != 0 :
            return False
    return True

# fonction qui permet d'afficher une sorte d'animation si le joueur gagne (c'est un GIF de Mr.Bean )
def animation_gagner():
    fc =20
    frames = [PhotoImage(file="images/GG.gif", format='gif -index %i' %(i))for i in range(fc)]
    def update(ind):
        frame  = frames[ind]
        ind +=1
        if ind == fc :
            ind =0
        pan.configure(image =frame)
        gagne.after(100,update,ind)
    def nv():
        gagne.destroy()
        nouv_Partie()
        
    destruction()
    gagne = Toplevel(root)
    # Afficher une fenêtre avec deux boutton "REJOUER" et "QUITTER"
    gagne .geometry("500x500+700+250")
    ima = PhotoImage(file="images/dem.png")
    pane = Label(gagne , image=ima)
    pane.pack()
    pane.image = ima
    pan = Label(gagne )
    pan.place(x =110,y = 40)
    gagne.after(0,update,0)
    fon = tkfont.Font(size = 20)
    cong = Label(gagne ,text = "Félicitation",bg = "white",fg = "blue",font = fon)
    cong.place(x =170,y = 250)
    btn0 = Button(  gagne  , text = " REJOUER",bg = "DodgerBlue2", command = nv  )
    btn0.place(x = 190, y = 320)
    btn0["border"] ="10"
    btn1 = Button(  gagne  , text = "  QUITER " ,bg = "DodgerBlue2" ,command = root.destroy )
    btn1.place(x = 190 , y = 390)
    btn1["border"] ="10"

# fonction appellèe qund on click sur une carte pour la deplacer  
def click(id,col):
    global etat
    global tap 
    global id1
    global col1 
    global sound 
    if tap == 0:
        if id != -1:
            id1 = id 
            col1 = col
            tap =1
    else:
        tap = 0
        if col == 8:
            return
        if id == -1 and col >8 and (id1 - 1)//4 == 0 :
            if(len(etat[col]) !=0):
               return
            etat[col].append(etat[col1].pop())
            if len(etat[col1]) != 0:
                x , y = etat[col1].pop()
                etat[col1].append((x,"apparu"))
            if gagner():
                animation_gagner()
            else:
                destruction()
                son_carte()
                affichage()
            return
        if col >8 and (id1 - 1)//4 != 0:
            if id == -1:
                return 
            if (id1 - 1)//4  != len(etat[col]):
                return
            z, t = etat[col1][len(etat[col1])-1]
            if z %4 != id %4:
                return
            etat[col].append(etat[col1].pop())
            if len(etat[col1]) != 0:
                x, y = etat[col1].pop()
                etat[col1].append((x,"apparu"))
            if gagner():
                animation_gagner()
                return 
            else:
                destruction()
                son_carte()
                affichage()
        
        if id == -1 and (id1-1) //4 == 12:
            a = etat[col1].pop()
            id2 , t = a
            l = []
            l.append(a)
            while  id2 != id1 :
                a = etat[col1].pop()
                id2 , t = a
                l.append(a)

            while(len(l) != 0):
                etat[col].append(l.pop())
                
            if len(etat[col1]) != 0:
                x , y  = etat[col1].pop()
                y  = "apparu"
                etat[col1].append((x,y))
        else:
            
            if mouvement_accepter(id1,id):
                a = etat[col1].pop()
                id2 , et = a
                l = []
                l.append(a)
                while  id2 != id1 :
                    a = etat[col1].pop()
                    id2 , et = a
                    l.append(a)
          
                while(len(l) != 0):
                    etat[col].append(l.pop())
                if len(etat[col1]) != 0:
                    x , y  = etat[col1].pop()
                    y = "apparu"
                    etat[col1].append((x,y))
                son_carte()
        if gagner():
            animation_gagner()
        else:
            destruction()
            affichage()
        id1 = 0

# fonction appelèe lors du démarage du programme pour l'initialisation des donnèes 
def init():
    global etat
    global sound 
    image_list = []
    for i in range(52):
        image_list.append(i+1)
    for i in range(7):
        lista = []
        for j in range (i+1):
            id = choice(image_list)
            image_list.pop(image_list.index(id))
            et = "cacher"
            if j == i:
               et = "apparu"
            lista.append((id,et))
        etat.append(lista)
    lista = []
    while len(image_list) != 0:
        id = choice(image_list)
        image_list.pop(image_list.index(id))
        lista.append((id,"cacher"))
    etat.append(lista)
    etat.append([])
    etat.append([])
    etat.append([])
    etat.append([])
    etat.append([])
    if sound:
        musicInit()
   
    
# fonction appelèe lors du click sur les cartes joker positionnèe à droite en bas
def click2():
    global etat
    id,et  =etat[7].pop()
    et = "apparu"
    etat[8].append((id,et))
    destruction()
    son_carte()
    affichage()

# fonction qui permet de rèafficher les cartes joker 
def reaffichage():
    global etat
    while len(etat[8])!= 0:
        id,et  =etat[8].pop()
        et = "cacher"
        etat[7].append((id,et))
    musicInit()       
    destruction()
    affichage()

# fonction qui permet la dèstruction des deux tableau "tabbtn" et "tablabel"  
def destruction():
    while len (tabbtn) !=0:
        a = tabbtn.pop()
        a.destroy()
    while len (tablabel) !=0:
        a = tablabel.pop()
        a.destroy()

# fonction principale qui permet l'affichage des cartes a l'ecran
# en utilisant le tableau etat
def affichage():
    global etat
    image = PhotoImage(file="images/b1fv.png")
    for i in range (7):
        y = 30
        image3 = PhotoImage(file="images/carte .png")
        btn = Button(root,image = image3)
        btn.place(x =i*150 + 300, y = y)
        btn.image = image3
        tabbtn.append(btn)
        btn ['command'] = lambda a = -1 ,b = i : click(a,b)
        for j in range(len(etat[i]) ):
            id , et = etat[i][j]
            if et == "cacher":
                label = Label(root,image = image)
                label.place(x = i*150 + 300 , y = y)
                label.image = image
                tablabel.append(label)
            else:
                s = "images/"+str(id)+".png"
                image2 = PhotoImage(file=s)
                btn = Button(root,image = image2)
                btn.place(x =i*150 + 300, y = y)
                btn.image = image2
                
                tabbtn.append(btn)
                btn ['command'] = lambda a = id ,b = i : click(a,b)
            y=y+20

    if len(etat[8])!= 0:
        image3 = PhotoImage(file="images/carte .png")
        btn = Button(root ,image = image3,command = reaffichage)
        btn.place(x =1500, y = 500)
        btn.image = image3
        tabbtn.append(btn)
    if  len(etat[7]) != 0 :
        btn = Button(root ,image = image,command = click2)
        btn.place(x =1500, y = 500)
        btn.image = image
        tabbtn.append(btn)
    if len(etat[8]) != 0 :
        id  ,et = etat[8][(len(etat[8])-1)]
        s = "images/"+str(id)+".png"
        image2 = PhotoImage(file=s)
        btn = Button(root,fg = "white" ,image = image2)
        btn.place(x =1200, y = 500)
        btn.image = image2
        tabbtn.append(btn)
        btn ['command'] = lambda a = id ,b = 8 : click(a,b)
    for i in [9,10,11,12]:
        image3 = PhotoImage(file="images/carte .png")
        btn = Button(root ,image = image3)
        btn.place(x =300+(i-9)*100, y = 500)
        btn.image = image3
        tabbtn.append(btn)
        btn ['command'] = lambda a = -1 ,b = i : click(a,b)
        if len(etat[i]) != 0:
            id  ,et = etat[i][(len(etat[i])-1)]
            s = "images/"+str(id)+".png"
            image2 = PhotoImage(file=s)
            btn = Button(root,fg = "white" ,image = image2)
            btn.place(x =300+(i-9)*100, y = 500)
            btn.image = image2
            tabbtn.append(btn)
            btn ['command'] = lambda a = id ,b = i : click(a,b)

# fonction qui permet de vèrifier si le deplacement des cartes 
# est possible ou pas 
def mouvement_accepter(id1,id2):
    global etat
    if (((id1-1) // 4)+1 == ((id2-1) // 4)) and (( id1 %4 in([3,0]) and id2 % 4 in([1,2])) or  ( id1 %4 in([1,2]) and id2 % 4 in([3,0]))):
        return True
    return False

# fonction appelèe pour crèer une nouvelle partie
def nouv_Partie():
    global etat
    global Recomancer
    while len(etat) != 0:
        etat.pop()
    while len(Recomancer) != 0:
        Recomancer.pop()
    init()
    i = 0
    for a  in etat:
        Recomancer.append([])
        for k in a:
            Recomancer[i].append(k)
        i+=1
    destruction()
    affichage()

# fonction qui permet de recharger le partie prècedent
def recharger():
    global etat
    global Recomancer
    while len(etat) != 0:
        etat.pop()
    for a  in Recomancer:
        etat.append(list(a))
    destruction()
    affichage()

# fonction appelèe au dèmarage du programme pour 
# afficher la fenêtre d'acceuil 
def demarage():
    def play():
        if musique :
            musique_Fond()
        nouv_Partie()
        top.destroy()
    # fonction qui permet d'afficher les fonds possibles
    def fond():
        def fun(event):
            global panel1
            s ="images/"+ck.get()+".png"
            im = PhotoImage(file = s)
            panel1.config(image = im)
            panel1.image = im
        top1 = Toplevel(top)
        top1.geometry("300x200+800+350")
        top1.protocol("WM_DELETE_WINDOW",disable_event)
        imag = PhotoImage(file="images/dem.png")
        pane = Label(top1, image=ima)
        pane.pack()
        pane.image = imag
        btn4 = Button(top1,text = "appliquer",command =top1.destroy )
        btn4.place(x = 100,y =100)
        ck = ttk.Combobox(top1,values = [
                "fond1",
                "fond2",
                "fond3",
                "fond4",
                "fond5",
                "fond6",
                "fond7",
                "fond8",
                "fond9",
        ])
        ck.current(0)
        ck.bind("<<ComboboxSelected>>",fun)
        ck.place(x=60,y=20)

    #fonction qui permet d'activer ou de dèsactiver le son 
    def son():
        def cb():
            global sound
            if var1.get() == 0:
                sound = False
            else: 
                sound = True
        def cb2():
            global musique
            if var2.get() == 0:
                musique = False
            else: 
                musique = True
        top2 = Toplevel(top)
        top2.geometry("300x200+800+350")
        imag = PhotoImage(file="images/dem.png")
        pane = Label(top2, image=ima)
        pane.pack()
        pane.image = imag
        var1 = IntVar()
        cSon = Checkbutton(top2,text  ="           SON DES CARTES         ",variable  =var1,command = cb , bg = "orange")
        cSon.select()
        cSon.place(x = 60 , y =20)
        var2 = IntVar()
        cMusic = Checkbutton(top2,text  ="                MUSIQUE        ",variable  =var2,command = cb2 , bg = "orange")
        cMusic.select()
        cMusic.place(x = 60 , y =100)
        
    # fonction qui permet de dèsactiver le exit de la fenêtre
    def disable_event():
        pass

    top = Toplevel(root)
    top.protocol("WM_DELETE_WINDOW",disable_event)
    top.geometry("500x500+700+250")
    ima = PhotoImage(file="images/dem.png")
    pane = Label(top, image=ima)
    pane.pack()
    btn1 = Button(top,text = "           JOUER           ",bg = "green",command = play)
    btn1["border"] ="10"
    btn1.place(x=170,y=100)
    btn2 = Button(top,text = "           FOND            ",bg = "green",command = fond)
    btn2["border"] ="10"
    btn2.place(x=170,y=190)
    btn3 = Button(top,text = "            SON             ",bg = "green",command = son)
    btn3["border"] ="10"
    btn3.place(x=170,y=280)
    top.focus_set()
    top.grab_set()
    top.transient(root)
    top.wait_window(root)

pygame.mixer.init()
menubar = Menu(root)
root.config(menu=menubar)
menujeux = Menu(menubar,tearoff=0)
menubar.add_cascade(label="Option", menu=menujeux)
menujeux.add_command(label="nouvelle partie ",command = nouv_Partie) 
menujeux.add_command(label="Recommencer ",command =recharger)
menujeux.add_separator() 
menujeux.add_command(label="Quitter ",command =root.destroy)              
image1 = PhotoImage(file="images/fond1.png")
panel1 = Label(root, image=image1)
panel1.pack(side='top')
panel1.image = image1

root.after(50,demarage)

root.mainloop()